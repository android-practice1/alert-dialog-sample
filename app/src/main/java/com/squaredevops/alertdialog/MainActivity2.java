package com.squaredevops.alertdialog;

import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity2 extends AppCompatActivity {

    EditText editText;
    Button button;
    TextView textView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);

        editText = findViewById(R.id.input);
        button = findViewById(R.id.button);
        textView = findViewById(R.id.output);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                // getting user input
                String userInput = editText.getText().toString();

                // converting string to integer
                int value=0;
                if (!"".equals(userInput)){
                    value = Integer.parseInt(userInput);
                }

                if (value < 5) {
                    textView.setText(value + " < 5");
                } else {
                    CustomAlertDialog("Error", "Make sure you've entered < 5");
                }

            }
        }); // end of button action
    }

    public void CustomAlertDialog(String title, String message) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setCancelable(false)
                .setIcon(R.drawable.alert_icon)
                .setTitle(title)
                .setMessage(message)
                .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.cancel();
                        //dialogInterface.cancel();
                    }
                }).show();
    }
}