# Alert Dialog

Implementation
```java
// creating builder
AlertDialog.Builder builder = new AlertDialog.Builder(this);

builder.setTitle("Error")
        .setCancelable(true)
        .setMessage("You must enter < 5, You've entered '" + value + "'.")
        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
    @Override
    public void onClick(DialogInterface dialogInterface, int i) {
        dialogInterface.cancel();
    }
});

//Creating dialog box
AlertDialog alert = builder.create();
alert.show();
```
**Status**
## TESTED AND OK

Farhan Sadik <br>
Square Development Group
